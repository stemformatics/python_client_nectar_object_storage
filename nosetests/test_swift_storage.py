import sys
sys.path.insert(0, '../hpc_dev_dataset_storage/model')
from nose.tools import assert_equal, assert_not_equal, assert_raises, assert_true, assert_false
from auth import SwiftStorageModel 
from config import *

def test_get_connected_with_swift_client():
	
	dataset = SwiftStorageModel
	msg = dataset.get_connected_with_swift_client(swift_storage_conn_user,swift_storage_conn_key,swift_storage_conn_project)

	#assert msg == "Get Connection Successful"
	assert_true(msg)

def test_unsuccessful_get_connected_with_swift_client():
	
	dataset = SwiftStorageModel
	msg = SwiftStorageModel.get_connected_with_swift_client(swift_storage_conn_user_dummy,swift_storage_conn_key_dummy,swift_storage_conn_project_dummy)
	
	assert_false(msg)

"""
def test_create_container():
	dataset = SwiftStorageModel
	msg = SwiftStorageModel.create_container(swift_storage_conn_user,swift_storage_conn_key,swift_storage_conn_project,'testing_container')
	
	assert_true(msg)

def test_delete_container():
	dataset = SwiftStorageModel
	msg = SwiftStorageModel.delete_container(swift_storage_conn_user,swift_storage_conn_key,swift_storage_conn_project,'s4m-portal-objects2')
	
	#assert msg == "Container:testing_container is deleted"
	assert_true(msg)

"""

def test_get_objects_in_container():
	dataset = SwiftStorageModel
	msg = SwiftStorageModel.get_objects_in_container(swift_storage_conn_user,swift_storage_conn_key,swift_storage_conn_project,'testing_container')
	
	assert_true(msg)
"""
def test_delete_objects_from_container():
	dataset= SwiftStorageModel
	msg = SwiftStorageModel.delete_objects_from_container(swift_storage_conn_user,swift_storage_conn_key,swift_storage_conn_project,'s4m-portal-objects2')

	assert_true(msg)

"""

"""
   

def test_store_container_id():

def test_mapping_containerid_with_datasetid():

def test_read_file_from_container():


def test_delete_mapping():
"""

