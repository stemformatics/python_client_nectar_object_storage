# README #


This repository is made to develop and test all the python scripts for pushing datasets 
and meta data from stemformatics HPC (pipeline) server to NeCTAR Swift storage.


### How do I get set up? ###
git clone the repository
For testing : Run 'nosetests test_swift_storage.py' (on your virtual env terminal)

You will require following packages to install
1. First check by running the command 'pip freeze' on terminal to check installed packages on your virtual env. Recommended Python version is : Python 2.6 or above 
2. Install the package by 'pip install python-swiftclient' or try with sudo
3. Install the package by 'pip install python-keystoneclient' or try with sudo
4. Install the package by 'pip install nose' or try with sudo for Python Nose Unit Testing

### Contribution guidelines ###

* The owner of this repo is Sadia Waleem, Rowland Mosbergen (Stemformatics)
* For further questions: contact Sadia(https://bitbucket.org/Sadia75) or Rowland (https://bitbucket.org/rowlandm/)