import sys
#import os
sys.path.insert(0, '../hpc_dev_dataset_storage')
from swiftclient import client
from swiftclient.exceptions import ClientException
from config import *

class SwiftStorageModel:
  @classmethod
  def get_connected_with_swift_client(cls,conn_user,conn_key,conn_project):
    try:
      
      swift = client.Connection(authurl='https://keystone.rc.nectar.org.au:5000/v3/',user=conn_user, 
        key=conn_key, tenant_name=conn_project, auth_version='3')
  
      
      #swift.put_container('new_container_name')
      for container in swift.get_account()[1]:
        print(container['name'])
            
      msg = True
    except ClientException as exp:
      #print (exp)
      msg = False
    return msg

  @classmethod
  def create_container(cls,conn_user,conn_key,conn_project,new_container_name):
    try:
      
      swift = client.Connection(authurl='https://keystone.rc.nectar.org.au:5000/v3/',user=conn_user, 
       key=conn_key, tenant_name=conn_project, auth_version='3')
  
      
      swift.put_container(new_container_name)
      for container in swift.get_account()[1]:
        print(container['name'])
            
      msg = True
    except ClientException as exp:
      msg = False
    return msg

  @classmethod
  def delete_container(cls,conn_user,conn_key,conn_project,existed_container_name):
    try:
      
      swift = client.Connection(authurl='https://keystone.rc.nectar.org.au:5000/v3/',user=conn_user, 
        key=conn_key, tenant_name=conn_project, auth_version='3')
  
      
      swift.delete_container(existed_container_name)
      for container in swift.get_account()[1]:
        print(container['name'])
            
      msg = True
    except ClientException as exp:
      #print(exp)
      msg = False

    return msg

  @classmethod
  def get_objects_in_container(cls,conn_user,conn_key,conn_project,existed_container_name):
    try:
      
      swift = client.Connection(authurl='https://keystone.rc.nectar.org.au:5000/v3/',user=conn_user, 
        key=conn_key, tenant_name=conn_project, auth_version='3')

      for data in swift.get_container(existed_container_name)[1]:
        print('{0}\t{1}\t{2}'.format(data['name'], data['bytes'], data['last_modified']))

      msg = True
  
      
    except ClientException as exp:
      
      msg = False
    return msg

  @classmethod
  def delete_objects_from_container(cls,conn_user,conn_key,conn_project,existed_container_name):
    try:
      
      swift = client.Connection(authurl='https://keystone.rc.nectar.org.au:5000/v3/',user=conn_user, 
        key=conn_key, tenant_name=conn_project, auth_version='3')

      for data in swift.get_container(existed_container_name)[1]:
        print('{0}\t{1}\t{2}'.format(data['name'], data['bytes'], data['last_modified']))
        swift.delete_object(existed_container_name, data['name'])

      msg = True

    except ClientException as exp:
      print(exp)
      msg = False
    return msg


if __name__ == '__main__':

  print("####............ Testing Swift Storage...................#########")

  #os.system('source SW-2017-openrc.sh')
  #os.system('swift post --header "X-Container-Read: .r:*" container2')

  #msg = SwiftStorageModel.get_connected_with_swift_client(swift_storage_conn_user, swift_storage_conn_key, swift_storage_conn_project)
  #msg = SwiftStorageModel.create_container(swift_storage_conn_user, swift_storage_conn_key, swift_storage_conn_project,'container2')

  #msg = SwiftStorageModel.get_objects_in_container(swift_storage_conn_user, 
   # swift_storage_conn_key, swift_storage_conn_project, "testing_container")

  #msg = SwiftStorageModel.delete_objects_from_container(swift_storage_conn_user, 
    #swift_storage_conn_key, swift_storage_conn_project, "s4m-portal-objects2")

  if (SwiftStorageModel.delete_container(swift_storage_conn_user,swift_storage_conn_key, swift_storage_conn_project, "new-container")!=True):
    print("container can't be deleted, it already has objects")


      